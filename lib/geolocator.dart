library geolocator;

import 'dart:async';
import 'dart:math';

import 'package:flutter/services.dart';
import 'package:google_api_availability/google_api_availability.dart';
import 'package:meta/meta.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:vector_math/vector_math.dart';



class Codec {
  static Map<String, dynamic> encodeLocationOptions(
      LocationOptions locationOptions) =>
      <String, dynamic>{
        'accuracy': locationOptions.accuracy.value,
        'distanceFilter': locationOptions.distanceFilter,
        'forceAndroidLocationManager':
        locationOptions.forceAndroidLocationManager,
        'timeInterval': locationOptions.timeInterval
      };
}


/// Contains detail location information.
class Position {
  Position({
             this.longitude,
             this.latitude,
             this.timestamp,
             this.mocked,
             this.accuracy,
             this.altitude,
             this.heading,
             this.speed,
             this.speedAccuracy,
           });
  
  Position._({
               this.longitude,
               this.latitude,
               this.timestamp,
               this.mocked,
               this.accuracy,
               this.altitude,
               this.heading,
               this.speed,
               this.speedAccuracy,
             });
  
  /// The latitude of this position in degrees normalized to the interval -90.0 to +90.0 (both inclusive).
  final double latitude;
  
  /// The longitude of the position in degrees normalized to the interval -180 (exclusive) to +180 (inclusive).
  final double longitude;
  
  /// The time at which this position was determined.
  final DateTime timestamp;
  
  ///Indicate if position was created from a mock provider.
  ///
  /// The mock information is not available on all devices. In these cases the returned value is false.
  final bool mocked;
  
  /// The altitude of the device in meters.
  ///
  /// The altitude is not available on all devices. In these cases the returned value is 0.0.
  final double altitude;
  
  /// The estimated horizontal accuracy of the position in meters.
  ///
  /// The accuracy is not available on all devices. In these cases the value is 0.0.
  final double accuracy;
  
  /// The heading in which the device is traveling in degrees.
  ///
  /// The heading is not available on all devices. In these cases the value is 0.0.
  final double heading;
  
  /// The speed at which the devices is traveling in meters per second over ground.
  ///
  /// The speed is not available on all devices. In these cases the value is 0.0.
  final double speed;
  
  /// The estimated speed accuracy of this position, in meters per second.
  ///
  /// The speedAccuracy is not available on all devices. In these cases the value is 0.0.
  final double speedAccuracy;
  
  @override
  String toString() {
    return 'Lat: $latitude, Long: $longitude';
  }
  
  /// Converts a collection of [Map] objects into a collection of [Position] objects.
  static List<Position> fromMaps(dynamic message) {
    if (message == null) {
      throw ArgumentError('The parameter \'message\' should not be null.');
    }
    
    final List<Position> list = message.map<Position>(fromMap).toList();
    return list;
  }
  
  /// Converts the supplied [Map] to an instance of the [Position] class.
  static Position fromMap(dynamic message) {
    if (message == null) {
      throw ArgumentError('The parameter \'message\' should not be null.');
    }
    
    final Map<dynamic, dynamic> positionMap = message;
    
    if (!positionMap.containsKey('latitude')) {
      throw ArgumentError.value(positionMap, 'positionMap',
          'The supplied map doesn\'t contain the mandatory key `latitude`.');
    }
    
    if (!positionMap.containsKey('longitude')) {
      throw ArgumentError.value(positionMap, 'positionMap',
          'The supplied map doesn\'t contain the mandatory key `longitude`.');
    }
    
    final DateTime timestamp = positionMap['timestamp'] != null
        ? DateTime.fromMillisecondsSinceEpoch(positionMap['timestamp'].toInt(),
        isUtc: true)
        : null;
    
    return Position._(
        latitude: positionMap['latitude'],
        longitude: positionMap['longitude'],
        timestamp: timestamp,
        mocked: positionMap['mocked'] ?? false,
        altitude: positionMap['altitude'] ?? 0.0,
        accuracy: positionMap['accuracy'] ?? 0.0,
        heading: positionMap['heading'] ?? 0.0,
        speed: positionMap['speed'] ?? 0.0,
        speedAccuracy: positionMap['speed_accuracy'] ?? 0.0);
  }
  
  Map<String, dynamic> toJson() => {
    'longitude': longitude,
    'latitude': latitude,
    'timestamp': timestamp?.millisecondsSinceEpoch,
    'mocked': mocked,
    'accuracy': accuracy,
    'altitude': altitude,
    'heading': heading,
    'speed': speed,
    'speedAccuracy': speedAccuracy,
  };
}


/// Contains detailed placemark information.
class Placemark {
  Placemark(
      {this.name,
        this.isoCountryCode,
        this.country,
        this.postalCode,
        this.administrativeArea,
        this.subAdministrativeArea,
        this.locality,
        this.subLocality,
        this.thoroughfare,
        this.subThoroughfare,
        this.position});
  
  Placemark._(
      {this.name,
        this.isoCountryCode,
        this.country,
        this.postalCode,
        this.administrativeArea,
        this.subAdministrativeArea,
        this.locality,
        this.subLocality,
        this.thoroughfare,
        this.subThoroughfare,
        this.position});
  
  /// The name of the placemark.
  final String name;
  
  /// The abbreviated country name, according to the two letter (alpha-2) [ISO standard](https://www.iso.org/iso-3166-country-codes.html).
  final String isoCountryCode;
  
  /// The name of the country associated with the placemark.
  final String country;
  
  /// The postal code associated with the placemark.
  final String postalCode;
  
  /// The name of the state or province associated with the placemark.
  final String administrativeArea;
  
  /// Additional administrative area information for the placemark.
  final String subAdministrativeArea;
  
  /// The name of the city associated with the placemark.
  final String locality;
  
  /// Additional city-level information for the placemark.
  final String subLocality;
  
  /// The street address associated with the placemark.
  final String thoroughfare;
  
  /// Additional street address information for the placemark.
  final String subThoroughfare;
  
  /// The geocoordinates associated with the placemark.
  final Position position;
  
  /// Converts a list of [Map] instances to a list of [Placemark] instances.
  static List<Placemark> fromMaps(dynamic message) {
    if (message == null) {
      throw ArgumentError('The parameter \'message\' should not be null.');
    }
    
    final List<Placemark> list = message.map<Placemark>(fromMap).toList();
    return list;
  }
  
  /// Converts the supplied [Map] to an instance of the [Placemark] class.
  static Placemark fromMap(dynamic message) {
    if (message == null) {
      throw ArgumentError('The parameter \'message\' should not be null.');
    }
    
    final Map<dynamic, dynamic> placemarkMap = message;
    
    return Placemark._(
      name: placemarkMap['name'] ?? '',
      isoCountryCode: placemarkMap['isoCountryCode'] ?? '',
      country: placemarkMap['country'] ?? '',
      postalCode: placemarkMap['postalCode'] ?? '',
      administrativeArea: placemarkMap['administrativeArea'] ?? '',
      subAdministrativeArea: placemarkMap['subAdministrativeArea'] ?? '',
      locality: placemarkMap['locality'] ?? '',
      subLocality: placemarkMap['subLocality'] ?? '',
      thoroughfare: placemarkMap['thoroughfare'] ?? '',
      subThoroughfare: placemarkMap['subThoroughfare'] ?? '',
      position: placemarkMap['position'] != null
          ? Position.fromMap(placemarkMap['position'])
          : null,
    );
  }
  
  Map<String, dynamic> toJson() => {
    'name': name,
    'isoCountryCode': isoCountryCode,
    'country': country,
    'postalCode': postalCode,
    'administrativeArea': administrativeArea,
    'subAdministrativeArea': subAdministrativeArea,
    'locality': locality,
    'subLocality': subLocality,
    'thoroughfare': thoroughfare,
    'subThoroughfare': subThoroughfare,
    'position': position
  };
}

/// Represents different options to configure the quality and frequency
/// of location updates.
class LocationOptions {
  const LocationOptions(
      {this.accuracy = LocationAccuracy.best,
        this.distanceFilter = 0,
        this.forceAndroidLocationManager = false,
        this.timeInterval = 0});
  
  /// Defines the desired accuracy that should be used to determine the location data.
  ///
  /// The default value for this field is [LocationAccuracy.best].
  final LocationAccuracy accuracy;
  
  /// The minimum distance (measured in meters) a device must move horizontally before an update event is generated.
  ///
  /// Supply 0 when you want to be notified of all movements. The default is 0.
  final int distanceFilter;
  
  /// Uses [FusedLocationProviderClient] by default and falls back to [LocationManager] when set to true.
  ///
  /// On platforms other then Android this parameter is ignored.
  final bool forceAndroidLocationManager;
  
  /// The desired interval for active location updates, in milliseconds (Android only).
  ///
  /// On iOS this value is ignored since position updates based on time intervals are not supported.
  final int timeInterval;
}

class LocationAccuracy {
  const LocationAccuracy._(this.value);
  
  final int value;
  
  /// Location is accurate within a distance of 3000m on iOS and 500m on Android
  static const LocationAccuracy lowest = LocationAccuracy._(0);
  
  /// Location is accurate within a distance of 1000m on iOS and 500m on Android
  static const LocationAccuracy low = LocationAccuracy._(1);
  
  /// Location is accurate within a distance of 100m on iOS and between 100m and
  /// 500m on Android
  static const LocationAccuracy medium = LocationAccuracy._(2);
  
  /// Location is accurate within a distance of 10m on iOS and between 0m and
  /// 100m on Android
  static const LocationAccuracy high = LocationAccuracy._(3);
  
  /// Location is accurate within a distance of ~0m on iOS and between 0m and
  /// 100m on Android
  static const LocationAccuracy best = LocationAccuracy._(4);
  
  /// Location accuracy is optimized for navigation on iOS and matches the
  /// [LocationAccuracy.best] on Android
  static const LocationAccuracy bestForNavigation = LocationAccuracy._(5);
  
  static const List<LocationAccuracy> values = <LocationAccuracy>[
    lowest,
    low,
    medium,
    high,
    best,
    bestForNavigation,
  ];
  
  static const List<String> _names = <String>[
    'lowest',
    'low',
    'medium',
    'high',
    'best',
    'bestForNavigation',
  ];
  
  @override
  String toString() => 'LocationAccuracy.${_names[value]}';
}

class GeolocationPermission {
  const GeolocationPermission._(this.value);
  
  final int value;
  
  /// Android: Fine and Coarse Location
  /// iOS: CoreLocation (Always and WhenInUse)
  static const GeolocationPermission location = GeolocationPermission._(0);
  
  /// Android: Fine and Coarse Location
  /// iOS: CoreLocation - Always
  static const GeolocationPermission locationAlways =
  GeolocationPermission._(1);
  
  /// Android: Fine and Coarse Location
  /// iOS: CoreLocation - WhenInUse
  static const GeolocationPermission locationWhenInUse =
  GeolocationPermission._(2);
  
  static const List<GeolocationPermission> values = <GeolocationPermission>[
    location,
    locationAlways,
    locationWhenInUse,
  ];
  
  static const List<String> _names = <String>[
    'location',
    'locationAlways',
    'locationWhenInUse',
  ];
  
  @override
  String toString() => 'GeolocationPermission.${_names[value]}';
}

class GeolocationStatus {
  const GeolocationStatus._(this.value);
  
  final int value;
  
  /// Permission to access the requested feature is denied by the user.
  static const GeolocationStatus denied = GeolocationStatus._(0);
  
  /// The feature is disabled (or not available) on the device.
  static const GeolocationStatus disabled = GeolocationStatus._(1);
  
  /// Permission to access the requested feature is granted by the user.
  static const GeolocationStatus granted = GeolocationStatus._(2);
  
  /// The user granted restricted access to the requested feature (only on iOS).
  static const GeolocationStatus restricted = GeolocationStatus._(3);
  
  /// Permission is in an unknown state
  static const GeolocationStatus unknown = GeolocationStatus._(4);
  
  static const List<GeolocationStatus> values = <GeolocationStatus>[
    denied,
    disabled,
    granted,
    restricted,
    unknown,
  ];
  
  static const List<String> _names = <String>[
    'denied',
    'disabled',
    'granted',
    'restricted',
    'unknown',
  ];
  
  @override
  String toString() => 'GeolocationStatus.${_names[value]}';
}

GeolocationStatus fromPermissionStatus(PermissionStatus status) {
  switch (status) {
    case PermissionStatus.denied:
      return GeolocationStatus.denied;
    case PermissionStatus.granted:
      return GeolocationStatus.granted;
    case PermissionStatus.restricted:
      return GeolocationStatus.restricted;
    default:
      return GeolocationStatus.unknown;
  }
}

LocationPermissionLevel toPermissionLevel(GeolocationPermission permission) {
  switch (permission) {
    case GeolocationPermission.locationAlways:
      return LocationPermissionLevel.locationAlways;
    case GeolocationPermission.locationWhenInUse:
      return LocationPermissionLevel.locationWhenInUse;
    default:
      return LocationPermissionLevel.location;
  }
}


/// Provides easy access to the platform specific location services (CLLocationManager on iOS and FusedLocationProviderClient on Android)
class Geolocator {
  factory Geolocator() {
    if (_instance == null) {
      const MethodChannel methodChannel =
          MethodChannel('flutter.baseflow.com/geolocator/methods');
      const EventChannel eventChannel =
          EventChannel('flutter.baseflow.com/geolocator/events');
      _instance = Geolocator.private(methodChannel, eventChannel);
    }
    return _instance;
  }

  @visibleForTesting
  Geolocator.private(this._methodChannel, this._eventChannel);

  static Geolocator _instance;

  final MethodChannel _methodChannel;
  final EventChannel _eventChannel;

  Stream<Position> _onPositionChanged;

  /// Returns a [Future] containing the current [GeolocationStatus] indicating the availability of location services on the device.
  Future<GeolocationStatus> checkGeolocationPermissionStatus(
      {GeolocationPermission locationPermission =
          GeolocationPermission.location}) async {
    final PermissionStatus permissionStatus = await LocationPermissions()
        .checkPermissionStatus(level: toPermissionLevel(locationPermission));

    return fromPermissionStatus(permissionStatus);
  }

  /// Returns a [bool] value indicating whether location services are enabled on the device.
  Future<bool> isLocationServiceEnabled() async {
    final ServiceStatus serviceStatus =
        await LocationPermissions().checkServiceStatus();

    return serviceStatus == ServiceStatus.enabled ? true : false;
  }

  /// On Android devices you can set [forceAndroidLocationManager]
  /// to true to force the plugin to use the [LocationManager] to determine the
  /// position instead of the [FusedLocationProviderClient]. On iOS this is ignored.
  bool forceAndroidLocationManager = false;

  GooglePlayServicesAvailability _googlePlayServicesAvailability;

  Future<bool> _shouldForceAndroidLocationManager() async {
    // By doing this check here, we save the App from always checking if Google Play Services
    // are available (which is not necessary if the developer wants to force the use of the LocationManager).
    if (forceAndroidLocationManager) {
      return true;
    }

    _googlePlayServicesAvailability ??= await GoogleApiAvailability.instance
        .checkGooglePlayServicesAvailability();

    return _googlePlayServicesAvailability !=
        GooglePlayServicesAvailability.success;
  }

  /// Returns the current position taking the supplied [desiredAccuracy] into account.
  ///
  /// When the [desiredAccuracy] is not supplied, it defaults to best.
  Future<Position> getCurrentPosition(
      {LocationAccuracy desiredAccuracy = LocationAccuracy.best,
      GeolocationPermission locationPermissionLevel =
          GeolocationPermission.location}) async {
    final PermissionStatus permission = await _getLocationPermission(
        toPermissionLevel(locationPermissionLevel));

    if (permission == PermissionStatus.granted) {
      final LocationOptions locationOptions = LocationOptions(
          accuracy: desiredAccuracy,
          distanceFilter: 0,
          forceAndroidLocationManager:
              await _shouldForceAndroidLocationManager());
      final Map<dynamic, dynamic> positionMap =
          await _methodChannel.invokeMethod('getCurrentPosition',
              Codec.encodeLocationOptions(locationOptions));

      try {
        return Position.fromMap(positionMap);
      } on ArgumentError {
        return null;
      }
    } else {
      _handleInvalidPermissions(permission);
    }

    return null;
  }

  /// Returns the last known position stored on the users device.
  ///
  /// On Android we look for the location provider matching best with the
  /// supplied [desiredAccuracy]. On iOS this parameter is ignored.
  /// When no position is available, null is returned.
  Future<Position> getLastKnownPosition(
      {LocationAccuracy desiredAccuracy = LocationAccuracy.best,
      GeolocationPermission locationPermissionLevel =
          GeolocationPermission.location}) async {
    final PermissionStatus permission = await _getLocationPermission(
        toPermissionLevel(locationPermissionLevel));

    if (permission == PermissionStatus.granted) {
      final LocationOptions locationOptions = LocationOptions(
          accuracy: desiredAccuracy,
          distanceFilter: 0,
          forceAndroidLocationManager:
              await _shouldForceAndroidLocationManager());
      final Map<dynamic, dynamic> positionMap =
          await _methodChannel.invokeMethod('getLastKnownPosition',
              Codec.encodeLocationOptions(locationOptions));

      try {
        return Position.fromMap(positionMap);
      } on ArgumentError {
        return null;
      }
    } else {
      _handleInvalidPermissions(permission);
    }

    return null;
  }

  /// Fires whenever the location changes outside the bounds of the [desiredAccuracy].
  ///
  /// This event starts all location sensors on the device and will keep them
  /// active until you cancel listening to the stream or when the application
  /// is killed.
  ///
  /// ```
  /// StreamSubscription<Position> positionStream = new Geolocator().GetPostionStream().listen(
  ///   (Position position) => {
  ///     // Handle position changes
  ///   });
  ///
  /// // When no longer needed cancel the subscription
  /// positionStream.cancel();
  /// ```
  ///
  /// You can customize the behaviour of the location updates by supplying an
  /// instance [LocationOptions] class. When you don't supply any specific
  /// options, default values will be used for each setting.
  Stream<Position> getPositionStream(
      [LocationOptions locationOptions = const LocationOptions(),
      GeolocationPermission locationPermissionLevel =
          GeolocationPermission.location]) async* {
    final PermissionStatus permission = await _getLocationPermission(
        toPermissionLevel(locationPermissionLevel));

    if (permission == PermissionStatus.granted) {
      _onPositionChanged ??= _eventChannel
          .receiveBroadcastStream(Codec.encodeLocationOptions(locationOptions))
          .map<Position>((dynamic element) =>
              Position.fromMap(element.cast<String, dynamic>()));

      yield* _onPositionChanged;
    } else {
      _handleInvalidPermissions(permission);
    }
  }

  Future<PermissionStatus> _getLocationPermission(
      LocationPermissionLevel locationPermissionLevel) async {
    final PermissionStatus permission = await LocationPermissions()
        .checkPermissionStatus(level: locationPermissionLevel);

    if (permission != PermissionStatus.granted) {
      final PermissionStatus permissionStatus = await LocationPermissions()
          .requestPermissions(permissionLevel: locationPermissionLevel);

      return permissionStatus;
    } else {
      return permission;
    }
  }

  void _handleInvalidPermissions(PermissionStatus permission) {
    if (permission == PermissionStatus.denied) {
      throw PlatformException(
          code: 'PERMISSION_DENIED',
          message: 'Access to location data denied',
          details: null);
    }
  }

  /// Returns a list of [Placemark] instances found for the supplied address.
  ///
  /// In most situations the returned list should only contain one entry.
  /// However in some situations where the supplied address could not be
  /// resolved into a single [Placemark], multiple [Placemark] instances may be returned.
  ///
  /// Optionally you can specify a locale in which the results are returned.
  /// When not supplied the currently active locale of the device will be used.
  /// The `localeIdentifier` should be formatted using the syntax: [languageCode]_[countryCode] (eg. en_US or nl_NL).
  Future<List<Placemark>> placemarkFromAddress(String address,
      {String localeIdentifier}) async {
    final Map<String, String> parameters = <String, String>{'address': address};
    if (localeIdentifier != null) {
      parameters['localeIdentifier'] = localeIdentifier;
    }

    final List<dynamic> placemarks =
        await _methodChannel.invokeMethod('placemarkFromAddress', parameters);
    return Placemark.fromMaps(placemarks);
  }

  /// Returns a list of [Placemark] instances found for the supplied coordinates.
  ///
  /// In most situations the returned list should only contain one entry.
  /// However in some situations where the supplied coordinates could not be
  /// resolved into a single [Placemark], multiple [Placemark] instances may be returned.
  ///
  /// Optionally you can specify a locale in which the results are returned.
  /// When not supplied the currently active locale of the device will be used.
  /// The `localeIdentifier` should be formatted using the syntax: [languageCode]_[countryCode] (eg. en_US or nl_NL).
  Future<List<Placemark>> placemarkFromCoordinates(
      double latitude, double longitude,
      {String localeIdentifier}) async {
    final Map<String, dynamic> parameters = <String, dynamic>{
      'latitude': latitude,
      'longitude': longitude
    };

    if (localeIdentifier != null) {
      parameters['localeIdentifier'] = localeIdentifier;
    }

    final List<dynamic> placemarks = await _methodChannel.invokeMethod(
        'placemarkFromCoordinates', parameters);

    try {
      return Placemark.fromMaps(placemarks);
    } on ArgumentError {
      return null;
    }
  }

  /// Convenience method to access [placemarkFromCoordinates()] using an
  /// instance of [Position].
  Future<List<Placemark>> placemarkFromPosition(Position position) =>
      placemarkFromCoordinates(position.latitude, position.longitude);

  /// Returns the distance between the supplied coordinates in meters.
  Future<double> distanceBetween(double startLatitude, double startLongitude,
          double endLatitude, double endLongitude) =>
      _methodChannel.invokeMethod<dynamic>('distanceBetween', <String, double>{
        'startLatitude': startLatitude,
        'startLongitude': startLongitude,
        'endLatitude': endLatitude,
        'endLongitude': endLongitude
      }).then<double>((dynamic result) => result);

  /// Returns the initial bearing between two points
  /// The initial bearing will most of the time be different than the end bearing, see [https://www.movable-type.co.uk/scripts/latlong.html#bearing]
  Future<double> bearingBetween(double startLatitude, double startLongitude,
      double endLatitude, double endLongitude) {
    var startLongtitudeRadians = radians(startLongitude);
    var startLatitudeRadians = radians(startLatitude);

    var endLongtitudeRadians = radians(endLongitude);
    var endLattitudeRadians = radians(endLatitude);

    var y = sin(endLongtitudeRadians - startLongtitudeRadians) *
        cos(endLattitudeRadians);
    var x = cos(startLatitudeRadians) * sin(endLattitudeRadians) -
        sin(startLatitudeRadians) *
            cos(endLattitudeRadians) *
            cos(endLongtitudeRadians - startLongtitudeRadians);

    return Future.value(degrees(atan2(y, x)));
  }
}
